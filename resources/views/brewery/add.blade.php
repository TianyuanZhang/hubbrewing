<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>HubBrewing</title>
    <link rel="stylesheet" href="/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="/css/style.css"/>
</head>
<body>
<div class="wrapper">
    @if(!empty(session('tip')))
    <script>
        $(function() {
            alert("{{session('tip')}}");
        });
    </script>
    @endif

    <div class="wrap-left">
        <div class="logo">
            <img src="/images/logo.png" alt="" srcset=""/>
        </div>
        <div class="nav-box" id="navList">
            <div class="line"></div>
            <div class="nav-list">
                <dl>
                    <dt>
                        <a href="###">Calendar <i></i></a>
                    </dt>
                    <dd>
                        <ul>
                            <li><a href="###">Daily calendar</a></li>
                            <li>
                                <a href="calendar.html"
                                >Weekly calendar</a
                                >
                            </li>
                            <li><a href="###">Monthly calendar</a></li>
                            <li><a href="###">Missing bottlings</a></li>
                            <li>
                                <a href="###">Closure days/holidays</a>
                            </li>
                        </ul>
                    </dd>
                </dl>
                <dl>
                    <dt><a href="###">Customers</a></dt>
                    <dd></dd>
                </dl>
                <dl>
                    <dt>
                        <a href="###">Brews and excise <i></i></a>
                    </dt>
                    <dd></dd>
                </dl>
                <dl class="open">
                    <dt>
                        <a href="###">Manage brewery info</a>
                    </dt>
                    <dd></dd>
                </dl>
                <dl>
                    <dt><a href="###">Manage Lists</a></dt>
                    <dd></dd>
                </dl>
            </div>
        </div>
    </div>
    <div class="wrap-right">
        <div class="right-head">
            <h4>Brewery information</h4>
        </div>
        <div class="right-con-box">
            <div class="con-box-inner">
                @if ($isEdit)
                <form action="{{ url('brewery/update') }}" method="post">
                    <input type="hidden" name="BreweryInfoID" value="{{ $breweryinfoInfo['BreweryInfoID'] ?? 0 }}">
                    @else
                    <form action="{{ url('brewery/store') }}" method="post">
                        @endif

                        @csrf
                        <div class="panel panel-box">
                            <div class="panel-heading">Identification</div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name"
                                            >Brewery name</label
                                            >
                                            <input
                                                type="text"
                                                name="BreweryName"
                                                class="form-control"
                                                placeholder=""
                                                value="{{ $breweryinfoInfo['BreweryName'] ?? '' }}"
                                            />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="name">ACN</label>
                                            <input
                                                type="text"
                                                name="ACN"
                                                class="form-control"
                                                placeholder=""
                                                value="{{ $breweryinfoInfo['ACN'] ?? '' }}"
                                            />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="name">ABN</label>
                                            <input
                                                type="text"
                                                name="ABN"
                                                class="form-control"
                                                placeholder=""
                                                value="{{ $breweryinfoInfo['ABN'] ?? '' }}"
                                            />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name"
                                            >Excise settlement permission
                                                number</label
                                            >
                                            <input
                                                type="text"
                                                name="ExciseSettlementPermissionNumber"
                                                class="form-control"
                                                placeholder=""
                                                value="{{ $breweryinfoInfo['ExciseSettlementPermissionNumber'] ?? '' }}"
                                            />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="name"
                                            >Client account number</label
                                            >
                                            <input
                                                type="text"
                                                name="ClientAccountNumber"
                                                class="form-control"
                                                placeholder=""
                                                value="{{ $breweryinfoInfo['ClientAccountNumber'] ?? '' }}"
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-box">
                            <div class="panel-heading">Contact details</div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="name">Phone</label>
                                            <input
                                                type="text"
                                                name="PhoneHome"
                                                class="form-control"
                                                placeholder=""
                                                value="{{ $contactinfoInfo['PhoneHome'] ?? '' }}"
                                            />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="name"
                                            >Email address</label
                                            >
                                            <input
                                                type="text"
                                                name="EmailAddress"
                                                class="form-control"
                                                placeholder=""
                                                value="{{ $contactinfoInfo['EmailAddress'] ?? '' }}"
                                            />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="name"
                                            >Website URL</label
                                            >
                                            <input
                                                type="text"
                                                name="WebsiteURL"
                                                class="form-control"
                                                placeholder=""
                                                value="{{ $breweryinfoInfo['WebsiteURL'] ?? '' }}"
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-box">
                            <div class="panel-heading">Address</div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name"
                                            >Address line1</label
                                            >
                                            <input
                                                type="text"
                                                name="AddressLine1"
                                                class="form-control"
                                                placeholder=""
                                                value="{{ $physicaladdressInfo['AddressLine1'] ?? '' }}"
                                            />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name">Suburb</label>
                                            <input
                                                type="text"
                                                name="Suburb"
                                                class="form-control"
                                                placeholder=""
                                                value="{{ $physicaladdressInfo['Suburb'] ?? '' }}"
                                            />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name"
                                            >Address line 2</label
                                            >
                                            <input
                                                type="text"
                                                name="AddressLine2"
                                                class="form-control"
                                                placeholder=""
                                                value="{{ $physicaladdressInfo['AddressLine2'] ?? '' }}"
                                            />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="name">Postcode</label>
                                            <input
                                                type="text"
                                                name="Postcode"
                                                class="form-control"
                                                placeholder=""
                                                value="{{ $physicaladdressInfo['Postcode'] ?? '' }}"
                                            />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="name">State</label>
                                            <input
                                                type="text"
                                                name="stateName"
                                                class="form-control"
                                                placeholder=""
                                                value="{{ $stateInfo['Name'] ?? '' }}"
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="btn-box text-right">
                            <button
                                class="btn btn-default btn-black"
                                type="reset"
                            >
                                Cancel
                            </button>
                            <button
                                class="btn btn-default btn-black"
                                type="submit"
                            >
                                Save
                            </button>
                        </div>
                    </form>
            </div>
        </div>
    </div>

</div>
<script src="/dist/js/jquery.min.js"></script>
<script src="/dist/js/bootstrap.min.js"></script>
<script src="/js/fun.js"></script>
</body>
</html>
