<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class BreweryController extends Controller
{
    public function index()
    {
        return 'hello world';
    }

    /**
     * 添加
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function add()
    {
        exit('您无权限该操作');

        return view('brewery.add', ['isEdit' => 0]);
    }

    /**
     * 修改
     *
     * @param $id
     */
    public function edit(int $id)
    {
        $stateInfo           = [];
        $physicaladdressInfo = [];
        $contactinfoInfo     = [];

        $breweryinfoInfo = DB::table('breweryinfo')
            ->select('BreweryInfoID', 'BreweryName', 'ACN', 'ABN', 'ExciseSettlementPermissionNumber','ClientAccountNumber', 'WebsiteURL', 'ContactInfoID')
            ->where('BreweryInfoID', $id)
            ->orderBy('BreweryInfoID', 'desc')
            ->first();
        if (!$breweryinfoInfo) {
            exit('参数有误!');
        }

        $breweryinfoInfo = get_object_vars($breweryinfoInfo);

        if ($breweryinfoInfo) {
            $contactinfoInfo = DB::table('contactinfo')
                ->select('ContactInfoID', 'PhoneHome', 'EmailAddress', 'PhysicalAddressID')
                ->where('ContactInfoID', $breweryinfoInfo['ContactInfoID'])
                ->orderBy('ContactInfoID', 'desc')
                ->first();

            if ($contactinfoInfo) {
                $contactinfoInfo = get_object_vars($contactinfoInfo);
            } else {
                $contactInfoID = DB::table('contactinfo')->insertGetId(
                    [
                        'PhoneHome'    => '',
                        'EmailAddress' => '',
                    ]
                );

                if ($contactInfoID) {
                    DB::table('breweryinfo')
                        ->where('BreweryInfoID', $breweryinfoInfo['BreweryInfoID'])
                        ->update(
                            [
                                'ContactInfoID' => $contactInfoID,
                            ]
                        );
                }

                $contactinfoInfo = DB::table('contactinfo')
                    ->select('ContactInfoID', 'PhoneHome', 'EmailAddress', 'PhysicalAddressID')
                    ->where('ContactInfoID', $contactInfoID)
                    ->orderBy('ContactInfoID', 'desc')
                    ->first();
                $contactinfoInfo = get_object_vars($contactinfoInfo);
            }
        }

        if ($contactinfoInfo) {
            $physicaladdressInfo = DB::table('physicaladdress')
                ->select('PhysicalAddressID', 'AddressLine1', 'AddressLine2', 'Suburb', 'Postcode', 'StateID')
                ->where('PhysicalAddressID', $contactinfoInfo['PhysicalAddressID'])
                ->orderBy('PhysicalAddressID', 'desc')
                ->first();

            if ($physicaladdressInfo) {
                $physicaladdressInfo = get_object_vars($physicaladdressInfo);
            } else {
                $physicalAddressID = DB::table('physicaladdress')->insertGetId(
                    [
                        'StateID'      => '',
                        'AddressLine1' => '',
                        'AddressLine2' => '',
                        'Suburb'       => '',
                        'Postcode'     => '',
                    ]
                );

                if ($physicalAddressID) {
                    DB::table('contactinfo')
                        ->where('ContactInfoID', $contactinfoInfo['ContactInfoID'])
                        ->update(
                            [
                                'PhysicalAddressID' => $physicalAddressID,
                            ]
                        );
                }

                $physicaladdressInfo = DB::table('physicaladdress')
                    ->select('PhysicalAddressID', 'AddressLine1', 'AddressLine2', 'Suburb', 'Postcode', 'StateID')
                    ->where('PhysicalAddressID', $physicalAddressID)
                    ->orderBy('PhysicalAddressID', 'desc')
                    ->first();
                $physicaladdressInfo = get_object_vars($physicaladdressInfo);

            }
        }

        if ($physicaladdressInfo) {
            $stateInfo = DB::table('state')
                ->select('StateID', 'Name')
                ->where('StateID', $physicaladdressInfo['StateID'])
                ->orderBy('StateID', 'desc')
                ->first();

            if ($stateInfo) {
                $stateInfo = get_object_vars($stateInfo);
            } else {
                $StateID = DB::table('state')->insertGetId(
                    [
                        'CountryID'    => '',
                        'Abbreviation' => '',
                        'Name'         => '',
                    ]
                );

                if ($StateID) {
                    DB::table('physicaladdress')
                        ->where('PhysicalAddressID', $physicaladdressInfo['PhysicalAddressID'])
                        ->update(
                            [
                                'StateID' => $StateID,
                            ]
                        );
                }

                $stateInfo = DB::table('state')
                    ->select('StateID', 'Name')
                    ->where('StateID', $StateID)
                    ->orderBy('StateID', 'desc')
                    ->first();
                $stateInfo = get_object_vars($stateInfo);
            }
        }

        return view('brewery.add', [
            'isEdit'              => 1,
            'stateInfo'           => $stateInfo,
            'physicaladdressInfo' => $physicaladdressInfo,
            'contactinfoInfo'     => $contactinfoInfo,
            'breweryinfoInfo'     => $breweryinfoInfo,
        ]);
    }

    /**
     * 保存入库
     *
     * @param Request $request
     *
     * @return \Illuminate\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        exit('您无权限该操作');
    }

    /**
     * 更新入库
     * @return \Illuminate\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request)
    {
        // breweryinfo
        $breweryInfoID = $request->input('BreweryInfoID');
        if (!$breweryInfoID) {
            exit('参数有误');
        }

        $preweryInfoIDInfo = DB::table('breweryinfo')
            ->select('BreweryInfoID', 'ContactInfoID')
            ->where('BreweryInfoID', $breweryInfoID)
            ->first();
        if (!$preweryInfoIDInfo) {
            exit('参数有误');
        }

        $breweryName                      = $request->input('BreweryName');
        $acn                              = $request->input('ACN');
        $abn                              = $request->input('ABN');
        $exciseSettlementPermissionNumber = $request->input('ExciseSettlementPermissionNumber');
        $clientAccountNumber              = $request->input('ClientAccountNumber');
        $websiteURL                       = $request->input('WebsiteURL');
        DB::table('breweryinfo')
            ->where('BreweryInfoID', $breweryInfoID)
            ->update(
                [
                    'BreweryName'                      => $breweryName,
                    'ACN'                              => $acn,
                    'ABN'                              => $abn,
                    'ExciseSettlementPermissionNumber' => $exciseSettlementPermissionNumber,
                    'ClientAccountNumber'              => $clientAccountNumber,
                    'WebsiteURL'                       => $websiteURL,
                ]
            );

        // contactinfo
        if ($preweryInfoIDInfo->ContactInfoID) {
            $phoneHome    = $request->input('PhoneHome');
            $emailAddress = $request->input('EmailAddress');

            $contactInfoIDInfo = DB::table('contactinfo')
                ->select('ContactInfoID', 'PhysicalAddressID')
                ->where('ContactInfoID', $preweryInfoIDInfo->ContactInfoID)
                ->first();

            if ($contactInfoIDInfo) {
                DB::table('contactinfo')
                    ->where('ContactInfoID', $contactInfoIDInfo->ContactInfoID)
                    ->update(
                        [
                            'PhoneHome'    => $phoneHome,
                            'EmailAddress' => $emailAddress,
                        ]
                    );
            }
        }

        // physicaladdress
        if ($contactInfoIDInfo->PhysicalAddressID) {
            $addressLine1 = $request->input('AddressLine1');
            $addressLine2 = $request->input('AddressLine2');
            $suburb       = $request->input('Suburb');
            $postcode     = $request->input('Postcode');

            $physicalAddressIDInfo = DB::table('physicaladdress')
                ->select('PhysicalAddressID', 'StateID')
                ->where('PhysicalAddressID', $contactInfoIDInfo->PhysicalAddressID)
                ->first();

            if ($physicalAddressIDInfo) {
                DB::table('physicaladdress')
                    ->where('PhysicalAddressID', $physicalAddressIDInfo->PhysicalAddressID)
                    ->update(
                        [
                            'AddressLine1' => $addressLine1,
                            'AddressLine2' => $addressLine2,
                            'Suburb'       => $suburb,
                            'Postcode'     => $postcode,
                        ]
                    );
            }
        }

        // state
        if ($physicalAddressIDInfo->StateID) {
            $stateName = $request->input('stateName');

            DB::table('state')
                ->where('StateID', $physicalAddressIDInfo->StateID)
                ->update(
                    [
                        'Name' => $stateName,
                    ]
                );
        }

        return redirect('/brewery/edit/'.$breweryInfoID)->with('tip', '修改成功');
    }
}
