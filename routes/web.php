<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// 啤酒厂
Route::get('/brewery/edit/{id}', 'BreweryController@edit')->where('id', '[0-9]+')->name('brewery.edit');

Route::post('/brewery/update', 'BreweryController@update');
